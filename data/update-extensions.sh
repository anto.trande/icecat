#!/bin/bash

set -e

for extension in submit-me privacy-redirect javascript-restrictor librifyjs-libgen-me-repack; do

  rm -rf /tmp/update-extension
  mkdir /tmp/update-extension
  (cd /tmp/update-extension
   wget -O extension.xpi https://addons.mozilla.org/firefox/downloads/latest/$extension/addon-$extension-latest.xpi
   unzip extension.xpi
   rm -f extension.xpi )

  if [ -f /tmp/update-extension/install.rdf ]; then
    ID=$(grep em:id /tmp/update-extension/install.rdf |sed 's/.*<em:id>//; s/<.*//' |head -n1)
  fi
  if [ -f /tmp/update-extension/manifest.json ]; then
    ID=$(grep '"id":' /tmp/update-extension/manifest.json |head -n1|cut -d \" -f 4)
  fi

  [ $extension = "use-google-drive-with-librejs" ] && ID="google_drive@0xbeef.coffee"
  [ -z $ID ] && ID=$extension"@extension"

  rm -rf extensions/$ID
  mv /tmp/update-extension extensions/$ID

done

for extension in librejs; do

  rm -rf /tmp/update-extension0
  mkdir /tmp/update-extension0
  (cd /tmp/update-extension0
   wget -O extension0.xpi https://ftp.gnu.org/gnu/$extension/$extension-7.21.1.xpi
   unzip extension0.xpi
   rm -f extension0.xpi )

  if [ -f /tmp/update-extension0/manifest.json ]; then
    ID=$(grep '"id":' /tmp/update-extension0/manifest.json |head -n1|cut -d \" -f 4)
  fi

  [ -z $ID ] && ID=$extension"@extension"
   rm -rf extensions/$ID
   mv /tmp/update-extension0 extensions/$ID
   find extensions/$ID -type f -name "*" -exec touch -a -m -t 202206111205.09 '{}' \;

done

find extensions -name cose.manifest -delete
find extensions -name cose.sig -delete
